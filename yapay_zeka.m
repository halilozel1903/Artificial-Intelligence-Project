ogrenme_verisi=fopen('ogrenme_database.txt','r'); % 50 y�ll�k verimiz dosyadan okundu
y=fscanf(ogrenme_verisi,'%g',[4,50]); % 4x50 lik matris g�sterildi
fclose(ogrenme_verisi); % dosyam�z kapat�ld�
y = y'; % matrisin transpozu al�nd�
y; % y ile de 4x50 li olan matris ekranda g�steriliyor


test_verisi=fopen('test_database.txt','r'); % 9 y�ll�k verimiz dosyadan okundu
y1=fscanf(test_verisi,'%g',[4,9]); % 4x9 luk matris y1 de�i�kenine atand�
fclose(test_verisi); % dosyay� kapatt�k
y1 = y1'; % matrisin transpozu al�nd� 
y1; % y1 ile de 4x9 luk olan matris ekranda g�steriliyor



data=y; % data de�i�kenimize dosyadaki matrisleri atad�k
test=y1; % test de�i�kenimize dosyadaki matrisler atand�

minimum = 1000000; % minimum de�er verildi.
min_sinif = 1; % min sinif de�eri olarak 1 verildi.

basarili_counter=0; % algoritman�n ba�ar�l� i�lemleri i�in bir counter tan�mlad�k
basarisiz_counter=0; % algoritman�n ba�ar�s�z i�lemleri i�in bir counter tan�mlad�k

for j=1:length(test(:,1)) % d��taki for da test verilerimizi 1 den ba�lay�p matrisin uzunlu�u kadar d�nd�r�yoruz
     test_verisi = test(j,:); % test verisi olarak j nin son elemanlar� ile i�leme tabi tutuyoruz

    for i=1:length(data(:,1)) % �klit mesafesi hesaplan�yor.
        fark = sqrt((data(i,1)-test_verisi(1))^2 + (data(i,2)-test_verisi(2))^2+(data(i,3)-test_verisi(3))^2);
        if (fark < minimum) % elde edilen fark minimumdan k���kse 
            minimum = fark; % fark� minumuma e�itledik.
            min_sinif = data(i,4);  % min s�n�f ise data daki de�er ile g�ncellenmi� oldu
        end % if kapatt�k
        
    end % for kapatt�k
    
    if (min_sinif==test_verisi(4)) % e�er min s�n�f test verisinin son eleman�na e�itse 
        basarili_counter=basarili_counter+1; % ba�ar�l� counter 1 artt�r�l�r
    else % e�er min s�n�f test verisinin son eleman�na e�it de�ilse
        basarisiz_counter=basarisiz_counter+1; % ba�ar�s�z counter 1 artt�r�l�r
        
    end % if kapat�ld�       
    
end % d��taki for kapat�ld�

basari_yuzde=(basarili_counter/(basarisiz_counter+basarili_counter))*100; 
% basar� y�zdesi hesab� yap�ld�.

basarisiz_yuzde = 100-basari_yuzde; % ba�ar�s�zl�k y�zdesi hesapland�

toplam_basari=sprintf('Ba�ar�l� i�lem : %d',basarili_counter); % basarili i�lem say�s� bir de�ere atand�.
toplam_basarisizlik=sprintf('Ba�ar�s�z i�lem : %d',basarisiz_counter);% basarisizlik say�s� bir de�ere atand�.
basari_orani = sprintf('Ba�ar� oran�: %%%3f', basari_yuzde); % ba�ar� oran� bir de�i�kene atand�.
basarisizlik_orani = sprintf('Ba�ar�s�z oran�: %%%3f', basarisiz_yuzde); % ba�ar�s�zl�k oran� bir de�i�kene atand�

disp(toplam_basari) % toplam basar� say�s� ekranda g�sterildi.
disp(toplam_basarisizlik) % toplam basar�s�zl�k say�s� ekranda g�sterildi.
disp(basari_orani) % basari_orani ekranda g�sterildi.
disp(basarisizlik_orani) % basarisizlik_orani ekranda g�sterildi.
